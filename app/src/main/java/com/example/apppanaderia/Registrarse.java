package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apppanaderia.DAO.UsuariosDAO;
import com.example.apppanaderia.Objetos.Usuarios;

import java.util.ArrayList;

public class Registrarse extends AppCompatActivity {
    Boolean valido = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registrarse);
        setUI();
    }

    private void setUI() {
        Button btCancelar,btRegistrar;
        EditText etNombre, etContrasenya, etApellidos;

        btCancelar = findViewById(R.id.btCancelarRegUsuario);
        btRegistrar = findViewById(R.id.btRegistrarUsuario);
        etNombre = findViewById(R.id.etRegNombre);
        etContrasenya = findViewById(R.id.etRegContra);
        etApellidos = findViewById(R.id.etRegApellidos);

        btCancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(getApplicationContext(), Login.class);
//                startActivity(intent);
                finish();

            }
        });

        btRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()){
                    if (!etNombre.getText().toString().equals("")){
                        if (!etContrasenya.getText().toString().equals("")){
                            if (!etApellidos.getText().toString().equals("")){

                                Usuarios user = new Usuarios();
                                user.setContraseña(etContrasenya.getText().toString());
                                user.setNombre(etNombre.getText().toString());
                                user.setApelldos(etApellidos.getText().toString());

                                comrpobarDatos(user);

                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }

                                if (!valido){
                                    registrarUser(user);
                                    Toast.makeText(getApplicationContext(),"Usuario Registrado",Toast.LENGTH_LONG).show();
                                    Intent intent = new Intent(getApplicationContext(), Login.class);
                                    startActivity(intent);
                                }else{
                                    Toast.makeText(getApplicationContext(),"Usuario ya registrado",Toast.LENGTH_LONG).show();
                                }

                            }else{
                                Toast.makeText(getApplicationContext(),"Porfavor Introduzca los apellidos",Toast.LENGTH_LONG).show();
                            }
                        }else{
                            Toast.makeText(getApplicationContext(),"Porfavor Introduzca la contraseña",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Porfavor Introduzca el nombre",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Revise su conexion",Toast.LENGTH_LONG).show();
                }

            }
        });

    }

    public void comrpobarDatos(Usuarios user){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UsuariosDAO userDAO = new UsuariosDAO();
                    valido = userDAO.findUser2(user);
                    Log.d("USERS", "USUARIO VALIDO");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public void registrarUser(Usuarios user){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UsuariosDAO userDAO = new UsuariosDAO();
                    userDAO.insert(user);
                    Log.d("USERS", "USUARIO REGISTRADO");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }

}