package com.example.apppanaderia;

import android.content.Intent;
import android.os.Bundle;

import com.example.apppanaderia.Adapters.VPAdapter;
import com.example.apppanaderia.Fragments.Embolsados;
import com.example.apppanaderia.Fragments.Panes;
import com.example.apppanaderia.Fragments.Todos;
import com.example.apppanaderia.databinding.ActivityMenuHorizontalBinding;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.example.apppanaderia.ui.main.SectionsPagerAdapter;

public class MenuHorizontal extends AppCompatActivity {

    private ActivityMenuHorizontalBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMenuHorizontalBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(this, getSupportFragmentManager());
        ViewPager viewPager = binding.viewPager;
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabs = binding.tabs;
        tabs.setupWithViewPager(viewPager);
        tabs.setTabMode(TabLayout.MODE_SCROLLABLE);




    }
    public void onClickVer(){

    }

//    public void onClickCrear(){
//        Intent intent = new Intent(getApplicationContext(), CrearPedido.class);
//        startActivity(intent);
//    }

}