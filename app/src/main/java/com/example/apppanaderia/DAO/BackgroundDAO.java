package com.example.apppanaderia.DAO;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.apppanaderia.Objetos.Productos;

import java.util.ArrayList;

public class BackgroundDAO  extends AsyncTask<String,String,String> {
    private static ArrayList<Productos> listaTodosProductos;

    private Context context;

    public BackgroundDAO(Context context){
        this.context = context;
    }

    @Override
    protected String doInBackground(String... strings) {
        ProductosDAO proDAO;
        try {
            proDAO = new ProductosDAO();
            listaTodosProductos = (ArrayList<Productos>) proDAO.findAll();

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }

}
