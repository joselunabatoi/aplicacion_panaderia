package com.example.apppanaderia.DAO;

import com.example.apppanaderia.Objetos.Pedir;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

public class PedirDAO implements  GenericDAO<Pedir> {
    final String SQLSELECTALL = "SELECT * FROM pedir";
    final String SQLINSERT = "insert into pedir(IDpedido, IDproducto, cantidad) VALUES (?, ?,?)";
    final String SQLSELECTPK = "SELECT * FROM pedidos WHERE id = ?";
    private final PreparedStatement pstSelectAll;
    private final PreparedStatement pstInsert;
    private final PreparedStatement pstSelectPK;

    public PedirDAO() throws Exception{
        Connection con = ConexionBD.getConexion();
        pstSelectAll = con.prepareStatement(SQLSELECTALL);
        pstInsert = con.prepareStatement(SQLINSERT, Statement.RETURN_GENERATED_KEYS);
        pstSelectPK = con.prepareStatement(SQLSELECTPK);
    }

    public void cerrar() throws SQLException {
        pstSelectAll.close();
        pstInsert.close();
        pstSelectPK.close();
    }


    @Override
    public Pedir findByPK(int id) throws Exception {
        return null;
    }

    @Override
    public List<Pedir> findAll() throws Exception {
        return null;
    }

    @Override
    public boolean insert(Pedir t) throws Exception {
        pstInsert.setInt(1, t.getIDprodcto());
        pstInsert.setInt(2, t.getIDpedido());
        pstInsert.setInt(3, t.getCantidad());
        int insertados = pstInsert.executeUpdate();
        return (insertados == 1);
    }

    @Override
    public Pedir insertGenKey(Pedir t) throws Exception {
        return null;
    }

    @Override
    public boolean update(Pedir t) throws Exception {
        return false;
    }

    @Override
    public boolean delete(int id) throws Exception {
        return false;
    }

    @Override
    public boolean delete(Pedir t) throws Exception {
        return false;
    }

    @Override
    public int size() throws Exception {
        return 0;
    }

    @Override
    public boolean exists(int id) throws Exception {
        return false;
    }

    @Override
    public List<Pedir> findByExample(Pedir t) throws Exception {
        return null;
    }
}
