package com.example.apppanaderia.DAO;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.example.apppanaderia.Objetos.Productos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SearchBy extends AsyncTask<String,String,String> {
    private ArrayList<Productos> productoslista = new ArrayList<>();
    private List<Productos> productos;
    ProgressDialog progressDialog;
    private Context context;

    public SearchBy(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Cargando...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected String doInBackground(String... strings) {
        ProductosDAO proDAO;
        try {
            proDAO = new ProductosDAO();
            productos = proDAO.findAll();
            productoslista.addAll(productos);

        }catch (Exception e){
            System.out.println(e.getMessage());
        }
        return null;
    }
}
