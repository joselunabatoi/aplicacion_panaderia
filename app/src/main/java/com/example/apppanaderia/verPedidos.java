package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apppanaderia.Adapters.AdapterPedidos;
import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.PedidosDAO;
import com.example.apppanaderia.Fragments.Pedido;
import com.example.apppanaderia.Objetos.Pedidos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class verPedidos extends AppCompatActivity {
    private RecyclerView recycler;
    private AdapterPedidos adapedidos;
    ArrayList<Pedidos> listapedidos = new ArrayList<>();
    List<Pedidos> pedidos = new ArrayList<>();
    ImageView imagenVacio;
    TextView textoVacio;
    FrameLayout fmProductos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_pedidos);
        setUI();
    }

    private void setUI() {
        recycler = findViewById(R.id.RecyclerPedidos);
        recycler.setLayoutManager(new LinearLayoutManager(this));

        if (isNetworkAvailable(getApplicationContext())){
            obtenerPedidos();
        }


        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        adapedidos = new AdapterPedidos(listapedidos,getApplicationContext());
        recycler.setAdapter(adapedidos);

    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }

    public void obtenerPedidos(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PedidosDAO pedDAO = new PedidosDAO();
                    pedidos = pedDAO.findByID(Login.usuario.getID());
                    listapedidos.addAll(pedidos);
                    Log.d("LISTA PEDIDOS", String.valueOf(listapedidos.size()));

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}