package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;

import com.example.apppanaderia.Adapters.AdapterElegir;
import com.example.apppanaderia.Adapters.AdapterProductosElegidos;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.Objetos.Pedir;
import com.example.apppanaderia.Objetos.Productos;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ListaProductosElegidos extends AppCompatActivity {
    private RecyclerView recyclerView;
    public static ArrayList<Productos> listaproductos = new ArrayList<>();
    public static ArrayList<Pedir> listapedir = new ArrayList<>();
    private AdapterProductosElegidos adapterProductosElegidos;

    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_productos_elegidos);
        setUI();
    }

    private void setUI() {
        FloatingActionButton fabAtras;

        fabAtras = findViewById(R.id.fabAtras);

        recyclerView = findViewById(R.id.RecyclerElegidos);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        listaproductos.clear();
        listapedir.clear();
        listapedir.addAll(AdapterElegir.listapedir);


        adapterProductosElegidos = new AdapterProductosElegidos(obtenerproductos(),getApplicationContext(),listapedir);
        recyclerView.setAdapter(adapterProductosElegidos);

        fabAtras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }

    public ArrayList<Productos> obtenerproductos(){
        ArrayList<Productos> listapro = new ArrayList<>();
        obtenerlista();
        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        for (int i = 0; i < listapedir.size(); i++) {
            for (int j = 0; j < listaproductos.size(); j++) {
                if (listapedir.get(i).getIDprodcto() == listaproductos.get(j).getID()){
                    listapro.add(listaproductos.get(j));
                }
            }
        }

        return listapro;
    }

    private void obtenerlista() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("hola", "hola ");
                try {

                    ProductosDAO proDAO = new ProductosDAO();
                    listaproductos.addAll(proDAO.findAll());

                } catch (SQLException e) {
                    Log.d("hola", e.getMessage());

                }
            }
        }).start();



    }


}