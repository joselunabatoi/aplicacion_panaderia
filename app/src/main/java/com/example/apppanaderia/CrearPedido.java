package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apppanaderia.Adapters.AdapterElegir;
import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.PedidosDAO;
import com.example.apppanaderia.DAO.PedirDAO;
import com.example.apppanaderia.Fragments.ListaProductos;
import com.example.apppanaderia.Fragments.Pedido;
import com.example.apppanaderia.Objetos.Pedidos;
import com.example.apppanaderia.Objetos.Pedir;
import com.example.apppanaderia.Objetos.Productos;

import java.util.ArrayList;
import java.util.Calendar;

public class CrearPedido extends AppCompatActivity {
    public static ArrayList<Productos> listaProductos = new ArrayList<>();
    private static ArrayList<Productos> productosPedidos = new ArrayList<>();
    Pedidos pedidoID;
    ArrayList<Pedir> listapedidos = new ArrayList<>();
    ArrayList<Float> precioproductos = new ArrayList<>();
    String tvtotal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_pedido);
        setUI();
    }

    private void setUI() {
        EditText etTelefono, etNombre;
        Button btElegir, btCancelarPed, btCrear,btVerSelecionados;
        DatePicker datepicker;
        TextView tvTotal;

        etTelefono = findViewById(R.id.etTelefonoPedido);
        etNombre = findViewById(R.id.etNombrePedido);
        btElegir = findViewById(R.id.btElegirProductos);
        btCrear = findViewById(R.id.btCrearPedido);
        datepicker = findViewById(R.id.DatePickerPedido);
        btVerSelecionados = findViewById(R.id.btVerListaSelect);
        tvTotal = findViewById(R.id.tvTotalPedido);
        btCancelarPed = findViewById(R.id.btCancelarPedido);

        //crear vista para lista de roductos selecionados
        precioproductos.clear();
        Log.d("LISTA PRECIO", String.valueOf(AdapterElegir.listapedir.size()));
        tvTotal.setText(calcularTotal());

        btCancelarPed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
                startActivity(intent);
            }
        });

        btElegir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ElegirProductos.listaProductos.clear();
                Intent intent = new Intent(getApplicationContext(),ElegirProductos.class);
                startActivity(intent);
            }
        });

        btVerSelecionados.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            Intent intent = new Intent(getApplicationContext(),ListaProductosElegidos.class);
            startActivity(intent);
            }
        });

        btCrear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable(getApplicationContext())){
                    if (!etNombre.getText().toString().equals("")){
                        if (!etTelefono.getText().toString().equals("")){

                            Calendar calendar = Calendar.getInstance();
                            int c = calendar.get(Calendar.DATE);
                            int a = calendar.get(Calendar.YEAR);
                            int b = calendar.get(Calendar.MONTH);
                            String d = Integer.valueOf(a).toString()+"-"+Integer.valueOf(b+1).toString()+"-"+Integer.valueOf(c).toString();

                            Pedidos pedidos = new Pedidos();
                            pedidos.setFechaentrega(obtenerFecha(datepicker));
                            pedidos.setIDusuario(Login.usuario.getID());
                            pedidos.setNombre(etNombre.getText().toString());
                            pedidos.setTelefono(Integer.parseInt(etTelefono.getText().toString()));
                            pedidos.setFechapedido(d);
                            pedidos.setEntregado(false);


                            try {
                                crearPedido(pedidos);
                                Thread.sleep(500);
                                obtenerpedido(Login.usuario.getID());
                                Thread.sleep(500);
                                listapedidos.addAll(AdapterElegir.listapedir);
                                Thread.sleep(500);
                                for (int i = 0; i < listapedidos.size(); i++) {
                                    listapedidos.get(i).setIDpedido(pedidoID.getID());
                                }
                                añadirPedir(listapedidos);
                                Thread.sleep(500);
                                AdapterElegir.listapedir.clear();
                                listapedidos.clear();
                                pedidoID = null;
                                precioproductos.clear();

                                Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
                                startActivity(intent);

                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }else{
                            Toast.makeText(getApplicationContext(),"Introduzca un telefono",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Introduzca un nombre",Toast.LENGTH_LONG).show();
                    }
                }else{
                    Toast.makeText(getApplicationContext(),"Revise su conexion",Toast.LENGTH_LONG).show();
                }

            }
        });


    }

    private void añadirPedir(ArrayList<Pedir> lista) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PedirDAO pedDAO = new PedirDAO();
                    for (int i = 0; i < lista.size(); i++) {
                        pedDAO.insert(new Pedir(lista.get(i).getIDpedido(),lista.get(i).getIDprodcto(),lista.get(i).getCantidad()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        }).start();
    }

    private void crearPedido(Pedidos pedido) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PedidosDAO pedDAO = new PedidosDAO();
                    pedDAO.insert(pedido);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void obtenerpedido(int id){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    PedidosDAO pedDAO = new PedidosDAO();
                    pedidoID = pedDAO.findLastPedido(id);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public String calcularTotal(){
        tvtotal = "";
        ArrayList<Pedir> pedir = new ArrayList<>(AdapterElegir.listapedir);
        Log.d("CALCULARTOTAL", String.valueOf(pedir.size()));
        listaProductos.clear();

        if (pedir.isEmpty()){
            tvtotal = "0.00€";
        }else{
            float total = 0;
            listaProductos.addAll(AdapterElegir.productos);
            for (int i = 0; i < pedir.size(); i++) {
                for (int j = 0; j < listaProductos.size(); j++) {
                    if (pedir.get(i).getIDprodcto() == listaProductos.get(j).getID()){
                        precioproductos.add(listaProductos.get(j).getPrecio());
                        Log.d("PRECIO POR ID", listaProductos.get(j).getID()+"    "+ listaProductos.get(j).getPrecio());
                    }
                }
            }
            Log.d("LISTA PRODPRECIO", String.valueOf(precioproductos.size()));
            for (int i = 0; i < pedir.size(); i++) {
                total += pedir.get(i).getCantidad() * precioproductos.get(i);
                Log.d("SUMAR TOTAL",total+" "+pedir.get(i).getCantidad()+" "+ precioproductos.get(i));
            }

            tvtotal = total+"€";
        }
        return tvtotal;
    }

    public String obtenerFecha(DatePicker datePicker){
        String Fecha;
        Fecha = String.valueOf(datePicker.getYear())+"-"+(datePicker.getMonth()+1)+"-"+datePicker.getDayOfMonth();
        return Fecha;
    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }

}