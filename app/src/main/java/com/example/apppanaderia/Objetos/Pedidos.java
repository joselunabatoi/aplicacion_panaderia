package com.example.apppanaderia.Objetos;

import java.util.Date;

public class Pedidos {
    private int ID;
    private int telefono;
    private String fechapedido;
    private String fechaentrega;
    private String nombre;
    private boolean entregado;
    private int IDusuario;

    public Pedidos(int ID, int telefono, String fechapedido, String fechaentrega, String nombre, int IDusuario, boolean entregado) {
        this.ID = ID;
        this.telefono = telefono;
        this.fechapedido = fechapedido;
        this.fechaentrega = fechaentrega;
        this.nombre = nombre;
        this.entregado = entregado;
        this.IDusuario = IDusuario;
    }

    public Pedidos(){

    }


    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getFechapedido() {
        return fechapedido;
    }

    public void setFechapedido(String fechapedido) {
        this.fechapedido = fechapedido;
    }

    public String getFechaentrega() {
        return fechaentrega;
    }

    public void setFechaentrega(String fechaentrega) {
        this.fechaentrega = fechaentrega;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isEntregado() {
        return entregado;
    }

    public void setEntregado(boolean entregado) {
        this.entregado = entregado;
    }

    public int getIDusuario() {
        return IDusuario;
    }

    public void setIDusuario(int IDusuario) {
        this.IDusuario = IDusuario;
    }
}
