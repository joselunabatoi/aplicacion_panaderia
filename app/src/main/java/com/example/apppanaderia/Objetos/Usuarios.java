package com.example.apppanaderia.Objetos;

public class Usuarios {
    private int ID;
    private String contraseña;
    private String nombre;
    private String apelldos;

    public Usuarios(int ID, String contraseña, String nombre, String apelldos) {
        this.ID = ID;
        this.contraseña = contraseña;
        this.nombre = nombre;
        this.apelldos = apelldos;
    }

    public Usuarios() {
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApelldos() {
        return apelldos;
    }

    public void setApelldos(String apelldos) {
        this.apelldos = apelldos;
    }
}
