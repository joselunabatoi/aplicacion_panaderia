package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apppanaderia.Adapters.AdapterElegir;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.Objetos.Pedir;
import com.example.apppanaderia.Objetos.Productos;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ElegirProductos extends AppCompatActivity {
    private RecyclerView recycler;
    private AdapterElegir adapProductos;
    public static ArrayList<Pedir> productospedidos;
    public static ArrayList<Productos> listaProductos =  new ArrayList<>();
    public static List<Productos> productos = new ArrayList<>();
    public static EditText etCantidad;
    Boolean comprobarlista = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_elegir_productos);
        setUI();
    }

    private void setUI()  {
        etCantidad = findViewById(R.id.etCantidadProductos);
        etCantidad.setInputType(InputType.TYPE_CLASS_NUMBER);

        recycler = findViewById(R.id.RecyclerProductos);
        recycler.setLayoutManager(new LinearLayoutManager(this));


        if (comprobarlista){
            if (isNetworkAvailable(getApplicationContext())){
                obetenerProductos();
            }else{
                Toast.makeText(getApplicationContext(),"No tiene conexion",Toast.LENGTH_LONG).show();
            }
        }

        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if (comprobarlista){
            adapProductos = new AdapterElegir(listaProductos,getApplicationContext());
            recycler.setAdapter(adapProductos);
            comprobarlista = false;
        }


    }



    private void obetenerProductos() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("hola", "hola ");
                try {

                    ProductosDAO proDAO = new ProductosDAO();
                    productos = proDAO.findAll();
                    listaProductos.addAll(productos);

                    Log.d("Lista ELEGIR ", String.valueOf(listaProductos.size()));

                } catch (SQLException e) {
                    Log.d("hola", e.getMessage());

                }
            }
        }).start();



    }

    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }
}