package com.example.apppanaderia.Fragments;

import android.content.Context;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.ConexionBD;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class Reposteria extends Fragment {

    RecyclerView recyclerpro;
    ArrayList<Productos> productoslista = new ArrayList<>();
    List<Productos> productos = new ArrayList<>();
    ImageView imagenVacio;
    TextView textoVacio;
    FrameLayout fmProductos;
    AdapterTodos adapater;
    Boolean comprobarlista = true;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_productos, container, false);

        imagenVacio = vista.findViewById(R.id.imagenVacio);
        textoVacio = vista.findViewById(R.id.textoVacio);
        fmProductos = vista.findViewById(R.id.FrameLayoutProductos);

        recyclerpro = vista.findViewById(R.id.RecyclerProductos);
        recyclerpro.setLayoutManager(new LinearLayoutManager(getContext()));

        if (comprobarlista){
            rellenarLista();
        }

        if (productoslista.isEmpty()){
            imagenVacio.setVisibility(View.VISIBLE);
            textoVacio.setVisibility(View.VISIBLE);
            fmProductos.setBackgroundColor(Color.GRAY);

        }else{
            adapater = new AdapterTodos(productoslista,getContext());
            recyclerpro.setAdapter(adapater);
            comprobarlista = false;

        }

        return vista;
    }

    private void rellenarLista(){
        productos.addAll(Todos.productoslista);
        for (int i = 0; i < productos.size(); i++) {
            if (productos.get(i).getCategoria().equals("reposteria")){
                productoslista.add(productos.get(i));
            }
        }
    }

}