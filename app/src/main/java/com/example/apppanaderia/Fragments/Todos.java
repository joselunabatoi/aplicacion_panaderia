package com.example.apppanaderia.Fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.ConexionBD;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.Login;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * A simple {@link Fragment} subclass.
 * create an instance of this fragment.
 */
public class Todos extends Fragment {

    RecyclerView recyclerpro;
    public static ArrayList<Productos> productoslista = new ArrayList<Productos>();
    ImageView imagenVacio;
    TextView textoVacio;
    FrameLayout fmProductos;
    List<Productos> productos;
    ExecutorService executor;
    Handler handler;
    AdapterTodos adapater;
    Boolean comprobarlista = true;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_lista_productos, container, false);

        imagenVacio = vista.findViewById(R.id.imagenVacio);
        textoVacio = vista.findViewById(R.id.textoVacio);
        fmProductos = vista.findViewById(R.id.FrameLayoutProductos);

        recyclerpro = vista.findViewById(R.id.RecyclerProductos);
        recyclerpro.setLayoutManager(new LinearLayoutManager(getContext()));

        productoslista.clear();

        if (isNetworkAvailable(getContext())){
            startBackgroundTask();
        }else{
            Toast.makeText(getContext(),"No tiene conexion",Toast.LENGTH_LONG).show();
        }


        try {
            Log.d("largo lista", String.valueOf(productoslista.size()));
            Thread.sleep(500);
            Log.d("largo lista sleep ", String.valueOf(productoslista.size()));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if (productoslista.isEmpty()){
            imagenVacio.setVisibility(View.VISIBLE);
            textoVacio.setVisibility(View.VISIBLE);
            fmProductos.setBackgroundColor(Color.GRAY);
        }else{
            adapater = new AdapterTodos(productoslista,getContext());
            recyclerpro.setAdapter(adapater);
            comprobarlista = false;
        }
        return vista;
    }


    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }


    private void startBackgroundTask() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.d("hola", "hola ");
                try {

                    ProductosDAO proDAO = new ProductosDAO();
                    productos = proDAO.findAll();
                    productoslista.addAll(productos);

                    Log.d("despues conexion ", String.valueOf(productoslista.size()));

                } catch (SQLException e) {
                    Log.d("hola", e.getMessage());

                }
            }
        }).start();

    }



    private void showToast(String msg){
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(),msg,Toast.LENGTH_LONG).show();
            }
        });
    }

}