package com.example.apppanaderia.Fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.apppanaderia.CrearPedido;
import com.example.apppanaderia.R;
import com.example.apppanaderia.verPedidos;

public class verPedidosFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_ver_pedidos, container, false);
        Button btVerPedidos, btCrearPedidos;

        btVerPedidos = view.findViewById(R.id.VerPedido);
        btCrearPedidos = view.findViewById(R.id.CrearPedido);

        btVerPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),verPedidos.class);
                startActivity(intent);
            }
        });

        btCrearPedidos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(),CrearPedido.class);
                startActivity(intent);
            }
        });

        return view;
    }

}