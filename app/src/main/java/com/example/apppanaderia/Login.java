package com.example.apppanaderia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.apppanaderia.Adapters.AdapterTodos;
import com.example.apppanaderia.DAO.ConexionBD;
import com.example.apppanaderia.DAO.ProductosDAO;
import com.example.apppanaderia.DAO.UsuariosDAO;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.Objetos.Usuarios;

import org.json.JSONException;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Login extends AppCompatActivity {
    public static Usuarios usuario;
    private EditText etNombre, etContrasenya;
    private ExecutorService executor;
    public static ArrayList<Usuarios>  usuariosPermitidos = new ArrayList<>();
    Boolean pasar = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setUI();
    }

    private void setUI() {
        Button btRegistrarse, btLogin;

        btRegistrarse = findViewById(R.id.btnRegister);
        btLogin = findViewById(R.id.btnLogin);
        etNombre = findViewById(R.id.etNombre);
        etContrasenya = findViewById(R.id.etContraseña);


        btRegistrarse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), Registrarse.class);
                startActivity(intent);
//                Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
//                startActivity(intent);
            }
        });

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()){
                    if (!etNombre.getText().toString().equals("")){
                        if (!etContrasenya.getText().toString().equals("")){

                            comprobarUsuario();
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            if (pasar){
                                saveUser();

                                Toast.makeText(getApplicationContext(),"Login realizado correctamente",Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getApplicationContext(),MenuHorizontal.class);
                                startActivity(intent);

                            }else{
                                Toast.makeText(getApplicationContext(),"Debe registrarse",Toast.LENGTH_LONG).show();
                            }


                        }else{
                            Toast.makeText(getApplicationContext(),"Porfavor introduzca la contraseña",Toast.LENGTH_LONG).show();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Porfavor Introduzca el nombre",Toast.LENGTH_LONG).show();
                    }

//                    startBackgroundTask();


                }
            }
        });


    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            Network nw = connectivityManager.getActiveNetwork();
            if (nw == null) return false;

            NetworkCapabilities actNw = connectivityManager.getNetworkCapabilities(nw);

            return actNw != null && (actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR));


        } else {
            NetworkInfo nwInfo = connectivityManager.getActiveNetworkInfo();
            return nwInfo != null && nwInfo.isConnected();

        }
    }



    private boolean comprobarUsuario() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UsuariosDAO userDAO = new UsuariosDAO();
                    Usuarios user = new Usuarios();
                    user.setNombre(etNombre.getText().toString());
                    user.setContraseña(etContrasenya.getText().toString());
                    pasar = userDAO.exists2(etNombre.getText().toString(),etContrasenya.getText().toString());
                } catch (Exception e) {
                    Log.d("Revisar USER",e.getMessage());
                }
            }
        }).start();
        return pasar;
    }

    private void saveUser(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    UsuariosDAO userDAO = new UsuariosDAO();
                    Usuarios user = new Usuarios();
                    user.setNombre(etNombre.getText().toString());
                    user.setContraseña(etContrasenya.getText().toString());
                    usuario = userDAO.findUser(user);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

}