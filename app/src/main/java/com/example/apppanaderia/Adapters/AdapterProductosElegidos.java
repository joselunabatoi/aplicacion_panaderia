package com.example.apppanaderia.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.apppanaderia.Objetos.Pedir;
import com.example.apppanaderia.Objetos.Productos;
import com.example.apppanaderia.R;

import java.util.ArrayList;

public class AdapterProductosElegidos extends RecyclerView.Adapter<AdapterProductosElegidos.MyViewHolder>{
    private Context context;
    private ArrayList<Productos> productos;
    public static ArrayList<Pedir> pedir;
    public static ArrayList<Pedir> pedir2;

    static class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPrecio, tvDescripcion, tvCantidad;
        ImageView imagen;
        Context cont;

        public MyViewHolder(@NonNull View view, ArrayList<Productos> productos, Context cont) {
            super(view);
            tvPrecio = view.findViewById(R.id.tvPrecioProducto);
            tvDescripcion = view.findViewById(R.id.tvNombreProducto);
            imagen = view.findViewById(R.id.imagenProducto);
            tvCantidad = view.findViewById(R.id.tvCantidadProdcutos);
            this.cont = cont;
        }


        public void bind(Productos productos,Pedir pedir1) {
            if (productos.getImagen().equals("sequillos_bolsa")) {
                this.imagen.setImageResource(R.drawable.sequillos_bolsa);
            } else if (productos.getImagen().equals("sequillos_sin_azucar_bolsa")) {
                this.imagen.setImageResource(R.drawable.sequillos_sin_azucar_bolsa);
            } else if (productos.getImagen().equals("barra_casera")) {
                this.imagen.setImageResource(R.drawable.barra_casera);
            } else if (productos.getImagen().equals("coca_harina")) {
                this.imagen.setImageResource(R.drawable.coca_harina);
            } else if (productos.getImagen().equals("coca_pizza")) {
                this.imagen.setImageResource(R.drawable.coca_pizza);
            } else if (productos.getImagen().equals("magdalenas_bombon")) {
                this.imagen.setImageResource(R.drawable.magdalenas_bombon);
            } else if (productos.getImagen().equals("magdalenas_casera")) {
                this.imagen.setImageResource(R.drawable.magdalenas_casera);
            } else if (productos.getImagen().equals("mantecado_almendra")) {
                this.imagen.setImageResource(R.drawable.mantecado_almendra);
            } else if (productos.getImagen().equals("mantecados")) {
                this.imagen.setImageResource(R.drawable.mantecados);
            } else if (productos.getImagen().equals("pan_cereales")) {
                this.imagen.setImageResource(R.drawable.pan_cereales);
            } else if (productos.getImagen().equals("rollos")) {
                this.imagen.setImageResource(R.drawable.rollos);
            } else if (productos.getImagen().equals("tonya")) {
                this.imagen.setImageResource(R.drawable.tonya);
            }

            this.tvDescripcion.setText(productos.getDescripcion());
            this.tvCantidad.setText(String.valueOf(pedir1.getCantidad()));
            String precio = String.valueOf(productos.getPrecio());
            this.tvPrecio.setText(precio + "€");


        }


    }





    public AdapterProductosElegidos(ArrayList<Productos> productos, Context context, ArrayList<Pedir> pedir) {
        this.productos = productos;
        this.context = context;
        this.pedir = pedir;
        this.pedir2 = pedir;
    }

    @NonNull
    @Override
    public AdapterProductosElegidos.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.ver_productos_mi_pedido, parent, false);
        return new AdapterProductosElegidos.MyViewHolder(itemLayout, productos, context);

    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(productos.get(position),pedir.get(position));
    }

    @Override
    public int getItemCount() {
        return productos.size();
    }
}
