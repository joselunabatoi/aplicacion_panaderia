package com.example.apppanaderia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NOMBRE = "panaderiaroque.db";
    public static final String TABLE_PRODUCTOS = "productos";
    public static final String TABLE_USUARIOS = "usuarios";
    public static final String TABLE_PEDIDOS = "pedidos";
    public static final String TABLE_PEDIR = "pedir";


    public DbHelper(@Nullable Context context) {
        super(context,DATABASE_NOMBRE,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE "+ TABLE_PRODUCTOS + "("+
                "ID INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "descripcion TEXT not null,"+
                "precio FLOAT not null,"+
                "categoria text not null,"+
                "imagen text not null)");

        db.execSQL("create table "+ TABLE_USUARIOS + "("+
                "ID integer primary key autoincrement,"+
                "contraseña text not null,"+
                "nombre text not null,"+
                "apellidos text not null)");

        db.execSQL("create table "+ TABLE_PEDIDOS + "("+
                "ID integer primary key autoincrement,"+
                "telefono integer not null,"+
                "fecha_pedido text not null,"+
                "fecha_entrega text not null,"+
                "nombre text not null,"+
                "IDusuario int,"+
                "entegado boolean,"+
                "foreign key (IDusuario) references "+TABLE_USUARIOS + "(ID))"
                );

        db.execSQL("create table "+ TABLE_PEDIR + "("+
                "IDpedido integer,"+
                "IDproductos integer,"+
                "cantidad integer,"+
                "foreign key (IDpedido) references "+TABLE_PEDIR+ "(ID),"+
                "foreign key (IDproductos) references "+TABLE_PRODUCTOS+ "(ID))"
                );



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("drop table "+ TABLE_PRODUCTOS);
        db.execSQL("drop table "+ TABLE_USUARIOS);
        db.execSQL("drop table "+ TABLE_PEDIDOS);
        db.execSQL("drop table "+ TABLE_PEDIR);
        onCreate(db);
    }
}
